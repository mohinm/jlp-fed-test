export function stripHtml(string) {
    return string.replace(/(<([^>]+)>)/ig, '');
}