import { stripHtml } from "./textUtils";

describe("Text Utilities", ()=> {

    it("should strip html tags from string", ()=> {
        const original = "<a><strong>Hello world<strong></a>";
        const expected = "Hello world";

        const result = stripHtml(original);
        expect(expected === result).toBeTruthy();
    });

});