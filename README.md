# UI Dev Technical Test - Dishwasher App

## Notes

 - Added a few Usability/a11y touches such as hover state on grid, alt text on images, hover state on product specification grid, fixed semantic headers

 - Broken down most of what was there into components and sub-components

 - Utilised SCSS imports for variable values for consistent measurements and colours

 - Assuming we're not targeting ie11 (not tested!)

 - Unit tests included a downgrade of React testing library in order to be compatible

 - Assumed that embedded html within json data was a security hazard and have stripped html before outputting text. Removed all dangerouslySetInnerHTML attributes.

 - Used a fast approach in embedding mockdata into the jsx Pages - could improve on this

 - Did not modify or fully implement ProductCarousel due to time constraints

 - Did not fully implement Read more section on Product page due to time constraints

 - Product page could be further componentised - responsive column layout component, componentised sections with headers, 

