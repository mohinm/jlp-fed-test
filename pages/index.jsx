import Head from "next/head";
import mockProductData from "../mockData/data.json";
import ProductListPage from "./product-list-page/product-list-page";

// export async function getServerSideProps() {
//   const response = await fetch(
//     "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
//   );
//   const data = await response.json();
//   return {
//     props: {
//       data: data,
//     },
//   };
// }

export  function getServerSideProps() {

  return {
    props: {
      data: mockProductData,
    },
  };
}

const Home = ({ data }) => {
  let items = data.products.slice(0,20);
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <ProductListPage title="Dishwashers" productItems={items}></ProductListPage>
    </div>
  );
};

export default Home;
