import HeadingLayout from "../../components/headingLayout/heading-layout";
import ProductList from "../../components/product-list/product-list";

const ProductListPage = ({title, productItems})=> {
    return (
        <HeadingLayout title={`${title} (${productItems.length})`}>
          <ProductList items={productItems} itemType={title}></ProductList>
       </HeadingLayout>
    );
}

export default ProductListPage;