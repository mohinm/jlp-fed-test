import HeadingLayout from "../../components/headingLayout/heading-layout";
import ProductCarousel from "../../components/product-carousel/product-carousel";
import ProductDetailsPrice from "../../components/product-details-price/product-details-price";
import mockProductDetailsData from "../../mockData/data2.json";
import styles from "./[id].module.scss";
import { stripHtml } from "../../utils/textUtils";
import ProductSpecificationList from "../../components/product-specification-list/product-specification-list";

// export async function getServerSideProps(context) {
//   const id = context.params.id;
//   const response = await fetch(
//     "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
//   );
//   const data = await response.json();

//   return {
//     props: { data },
//   };
// }


export  function getServerSideProps(context) {
  return {
    props: {
      data: mockProductDetailsData.detailsData.filter((details)=> {
        return details.productId === context.params.id;
      })[0],
    },
  };
}



const ProductDetail = ({ data }) => {
  return (
    <HeadingLayout title={data.title}>
      <div className={styles.productDatasheet}>
        <section className={styles.pageSection}>
          <ProductCarousel image={data.media.images.urls[0]} />
        </section>

        <section className={styles.pageSection}>
          <ProductDetailsPrice 
            price={data.price.now}
            specialOffer={data.displaySpecialOffer}
            services={data.additionalServices.includedServices}></ProductDetailsPrice>
        </section>

        <section className={styles.pageSection}>
          <div>
            <h2>Product information</h2>
            <p>{stripHtml(data.details.productInformation)}</p>
          </div>
          <h2>Product specification</h2>
          <ProductSpecificationList details={data.details.features[0]}></ProductSpecificationList>
        </section>
      </div>
    </HeadingLayout>
  );
};

export default ProductDetail;
