import styles from "./product-list-item.module.scss";

const ProductListItem = ({ image, price, description, alt }) => {
  return (
    <div className={styles.productItem}>
      <div>
        <img src={image} alt={alt} className={styles.productImage} />
      </div>
      <div className={styles.description}>{description}</div>
      <div className={styles.price}>{price}</div>
    </div>
  );
};

export default ProductListItem;
