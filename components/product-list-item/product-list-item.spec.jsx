import { render, screen } from "@testing-library/react";
import ProductListItem from "./product-list-item";
import '@testing-library/jest-dom';

describe("ProductListItem", ()=>{

  const item = {
    image: "myImage.png",
    title: "myTitle",
    price: "myPrice",
    alt: "myAlt"
  };

  beforeEach(()=> {
    render(<ProductListItem image={item.image} 
      description={item.title} 
      price={item.price} 
      alt={item.alt}>
    </ProductListItem>);
  });
  
    it('should render a product list item container', () => {
      expect(document.querySelector(".productItem")).toBeInTheDocument();
    });

    it('should render a product list item image and alt text', () => {
      expect(screen.getByRole("img").src).toContain(item.image);
      expect(screen.getByAltText(item.alt)).toBeInTheDocument();
    });

    it('should render a product list item description and price', () => {
      expect(document.querySelector(".description").textContent).toEqual(item.title);
      expect(document.querySelector(".price").textContent).toEqual(item.price);
    });

});

