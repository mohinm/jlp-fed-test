import '@testing-library/jest-dom';
import { render } from "@testing-library/react";
import ProductSpecificationList from './product-specification-list';

describe("ProductSpecificationList", ()=> {

    it("should render a list container", ()=>{
        const empty ={
            attributes: []
        }
        render(<ProductSpecificationList details={empty}></ProductSpecificationList>);
        expect(document.querySelector("ul.specList")).toBeInTheDocument();
    });


    it("should render a spec list with items", ()=>{
        const items ={
            attributes: [
                {
                    name: "myName1",
                    id: "myId1",
                    value: "val1"
                },
                {
                    name: "myName2",
                    id: "myId2",
                    value: "val2"
                }
            ]
        }
        render(<ProductSpecificationList details={items}></ProductSpecificationList>);
        expect(document.querySelectorAll("li.specListItem").length).toEqual(2);
    });

    it("should remove html from name", ()=>{
        const item ={
            attributes: [
                {
                    name: "<b>myName</b>",
                    id: "myId1",
                    value: "val1"
                }
            ]
        }
        render(<ProductSpecificationList details={item}></ProductSpecificationList>);
        expect(document.querySelector(".specName").innerHTML).toEqual("myName");
    });


});