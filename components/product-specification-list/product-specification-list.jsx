import { stripHtml } from "../../utils/textUtils";
import styles from "./product-specification-list.module.scss";

const ProductSpecificationList = ({details}) => {
    return (
        <ul className={styles.specList}>
        {details.attributes.map((item) => (
          <li className={styles.specListItem} key={item.id}>
            <div className={styles.specName}>{stripHtml(item.name)}</div>
            <div>{stripHtml(item.value)}</div>
          </li>
        ))}
      </ul>
    );
};

export default  ProductSpecificationList