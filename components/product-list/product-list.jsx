import styles from "./product-list.module.scss";
import Link from "next/link";
import ProductListItem from "../product-list-item/product-list-item"

const ProductList = ({items, itemType}) => {

    return (
    <ul className={styles.productList}>
        {items.map((item) => (
        <li className={styles.productListItem} key={item.productId}>
            <Link
            key={item.productId}
            href={{
                pathname: "/product-detail/[id]",
                query: { id: item.productId },
            }}
            >
            <a className={styles.link}>
                <ProductListItem 
                    image={item.image} 
                    price={item.price.now} 
                    description={item.title}
                    alt={`${itemType} - ${item.title}`}></ProductListItem>
            </a>
            </Link>
        </li>
        ))}
  </ul>);
}
export default ProductList;