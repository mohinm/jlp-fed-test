import { render, screen } from "@testing-library/react";
import ProductList from "./product-list";
import '@testing-library/jest-dom';

describe("ProductList", ()=> {


    const items = [
        {
            productId: "123",
            image: "myImage1",
            price: {
                now: "567"
            },
            title: "myTitle1"
        },
        {
            productId: "678",
            image: "myImage2",
            price: {
                now: "899"
            },
            title: "myTitle2"
        }
    ];

    beforeEach(()=> {
    });

    it("should render a list container", ()=> {
        render(<ProductList items={[]} itemType=""></ProductList>);
        expect(document.querySelector("ul.productList")).toBeInTheDocument();
    });


    it("should render a list item and link+href", ()=> {
        render(<ProductList items={items} itemType="balloons"></ProductList>);
        expect(document.querySelector("li.productListItem")).toBeInTheDocument();
        expect(document.querySelector("a.link")).toBeInTheDocument();
        expect(document.querySelector("a.link").href).toContain(`/product-detail/${items[0].productId}`);
    });

    it("should render productlistitems", ()=> {
        render(<ProductList items={items} itemType="kites"></ProductList>);
        expect(document.querySelectorAll(".productItem").length).toEqual(2);
        
    });



});