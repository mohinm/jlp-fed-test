import styles from "./product-details-price.module.scss"

const ProductDetailsPrice = ({price, specialOffer, services}) => {

    return (
        <div className={styles.priceContainer}>
            <div className={styles.price}>£{price}</div>
            <div className={styles.specialOffer}>{specialOffer}</div>
            <div className={styles.services}>{services}</div>
        </div>
    );
};

export default ProductDetailsPrice;