import '@testing-library/jest-dom';
import { render } from "@testing-library/react";
import ProductDetailsPrice from "./product-details-price";

describe("ProductDetailsPrice", ()=> {

    it("should render a price container", ()=> {
        render(<ProductDetailsPrice price="" specialOffer="" services=""></ProductDetailsPrice>);
        expect(document.querySelector(".priceContainer")).toBeInTheDocument();
    });


    it("should render a price container with all values", ()=> {
        const result = {
            price: "1222",
            specialOffer: "Wow!",
            services: "DIY"
        }
        render(<ProductDetailsPrice price={result.price} 
                    specialOffer={result.specialOffer} 
                    services={result.services}></ProductDetailsPrice>);
        expect(document.querySelector(".price").textContent).toContain(result.price);
        expect(document.querySelector(".specialOffer").textContent).toContain(result.specialOffer);
        expect(document.querySelector(".services").textContent).toContain(result.services);
    });
});