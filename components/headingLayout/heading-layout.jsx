import styles from "./heading-layout.module.scss";

const HeadingLayout = ({title, children}) => {

    return (
        <div className={styles.layout}>
            <h1>{title}</h1>
            <div>{children}</div>
        </div>
    );
}

export default HeadingLayout;