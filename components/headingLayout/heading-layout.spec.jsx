import { render, screen } from "@testing-library/react";
import HeadingLayout from "./heading-layout";
import '@testing-library/jest-dom';

describe("HeadingLayout", ()=> {

    it("should render a layout with given header title", ()=> {
        const result = "MyTitle"
        render(<HeadingLayout title={result}></HeadingLayout>);
        expect(document.querySelector("h1").textContent).toContain(result);
    });


    it("should render a layout with children embedded", ()=> {
        render(<HeadingLayout title="Hello"><p>World</p></HeadingLayout>);
        expect(document.querySelector("p")).toBeInTheDocument();
    });
});